﻿using CRUD.Crud;
using static System.Console;

namespace CRUD
{
    static class Program
    {
        static void Main()
        {

            /*
              Приклад команд:
                >>> add user -userId -firstname:name -lastname:name
                >>> delete user -userId
                >>> update user -userId -firstname:name
                >>> get user -userId

            set storage (xml or json)

            add <storage> -firstname <text> -lastname <text>

            delete <storage> -userId <id>

            get <storage> -userId <id>

            update <storage> -userId <id> (-firstname or -lastname)<new text>
             */


            
            ///uncomment here
            Application application = new Application();
             while (true)
             {
                 application.Execute(ReadLine());
             }
        }
    }
}
