﻿using System.Collections.Generic;
using CRUD.Objects;

namespace CRUD.Patterns.Adapter
{
    /// <summary>
    /// Unified interface for storages: XML-files, JSON-files, databases
    /// Each concrete type of storage implements this interface
    /// Client (Application class (?)) uses only its methods, not knowing about details of 
    /// concrete implementations
    /// </summary>
    public interface IStorage
    {
        /// <summary>
        /// List which contains data from file/database
        /// </summary>
        List<Person> People { get;}

        /// <summary>
        /// Creates a new line/row in file/database
        /// </summary>
        /// <param name="person">
        /// Person which will be added
        /// </param>
        void Add(Person person);

        /// <summary>
        /// Get one person from storage
        /// </summary>
        /// <param name="id">Unique id of person we are looking for</param>
        /// <returns>Found person</returns>
        Person Get(int id);

        /// <summary>
        /// Get all information from storage
        /// (If all info is stored in 'People' and is always up-to-date,
        /// is it needed to return List?)
        /// </summary>
        /// <returns>List of people stored in concrete storage</returns>
        List<Person> Read();

        /// <summary>
        /// Save all information to storage
        /// </summary>
        void Save();

        /// <summary>
        /// Updates existing record in list
        /// </summary>
        /// <param name="person">New data</param>
        /// <param name="id">Id of a person that will be updated</param>
        void Update(Person person, int id);

        /// <summary>
        /// Deletes selected user from storage
        /// </summary>
        /// <param name="id">Selected Id</param>
        void Delete(int id);
    }
}
