﻿using System.Collections.Generic;
using System.Linq;
using CRUD.Objects;

namespace CRUD.Patterns.Adapter
{
    /// <summary>
    /// Concrete implementation of IStorage for JSON-files
    /// Automatically saves changes to file (is it needed?)
    /// </summary>
    abstract class FileStorage: IStorage
    {
        private List<Person> _people;

        public List<Person> People
        {
            get
            {
                if (_people == null)
                {
                    Read();
                }
                return _people;
            }
            protected set { _people = value; }
        }

        public string Filename { get; }

        protected FileStorage(string filename)
        {
            Filename = filename;
        }

        public virtual void Add(Person person)
        {
            People.Add(person);
            Save();
        }

        public virtual Person Get(int id)
        {
            return People[id];
        }

        /// <summary>
        /// Reading and parsing file
        /// </summary>
        /// <returns></returns>
        public abstract List<Person> Read();

        public virtual void Update(Person person, int id)
        {
            int index = People.FindIndex(p => p.Id == id);
            People[index] = person;
            Save();
        }

        public virtual void Delete(int id)
        {
            var person = People.FirstOrDefault(p => p.Id == id);
            People.Remove(person);
            Save();
        }


        public abstract void Save();
    }
}
