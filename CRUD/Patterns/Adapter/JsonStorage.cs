﻿using System.Collections.Generic;
using System.IO;
using CRUD.Editors;
using CRUD.Objects;

namespace CRUD.Patterns.Adapter
{
    class JsonStorage: FileStorage
    {
        public JsonStorage(string filename) : base(filename)
        {
        }

        public override List<Person> Read()
        {
            if (Filename != null)
            {
                string jsonString = File.ReadAllText(Filename);
                People = (List<Person>)jsonString.Deserialize();
                return People;
            }
            return null;
        }

        public override void Save()
        {
            string jsonString = People.Serialize();
            File.WriteAllText(Filename, jsonString);
        }
    }
}
