﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD.Patterns.UsingCommand
{
    class Read_Command : Command
    {
        public Read_Command(Receiver receiver)
            :base(receiver)
        {

        }

        public override void Execute()
        {
            receiver.Get(arguments);
        }
    }
}
