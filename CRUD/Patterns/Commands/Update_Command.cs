﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD.Patterns.UsingCommand
{
    class Update_Command : Command
    {
        public Update_Command(Receiver receiver)
            :base(receiver)
        {

        }

        public override void Execute()
        {
            receiver.Update(arguments);
        }
    }
}
