﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRUD.Crud;
using static CRUD.Crud.Application;

namespace CRUD.Patterns.Interpreter
{
    public class SetJsonStorage : SetStorage
    {
        public override Tuple<Storage, string> Interpret()
        {
            Storage storageType = Storage.Json;
            string format = ".txt";
            return new Tuple<Storage, string>(storageType, format);
        }
    }
}
