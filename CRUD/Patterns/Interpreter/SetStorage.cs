﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CRUD.Crud.Application;

namespace CRUD.Patterns.Interpreter
{
    public abstract class SetStorage
    {
        public abstract Tuple<Storage, string> Interpret();
    }
}
