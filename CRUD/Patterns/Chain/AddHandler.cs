﻿

using CRUD.Patterns.UsingCommand;

namespace CRUD.Patterns.Chain
{
	class AddHandler : IChain
	{
		public bool Process(string command)
		{
			if (command == "add")
			{
				Client.CreateCommand();
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
