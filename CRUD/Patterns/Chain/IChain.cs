﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD.Patterns.Chain
{
	public interface IChain
	{
		bool Process(string command);
	}
}
