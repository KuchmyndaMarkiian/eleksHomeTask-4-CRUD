﻿using CRUD.Patterns.UsingCommand;

namespace CRUD.Patterns.Chain
{
	class GetHandler : IChain
	{
		public bool Process(string command)
		{
			if (command == "get")
			{
				Client.ReadCommand();
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
