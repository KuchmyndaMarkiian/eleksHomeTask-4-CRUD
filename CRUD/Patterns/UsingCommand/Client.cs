﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRUD.Objects;

namespace CRUD.Patterns.UsingCommand
{
    static class Client
    {
        private static Command _createCommand;
        private static Command _readCommand;
        private static Command _updateCommand;
        private static Command _deleteCommand;
        private static Invoker invoker;
        public static void ClientMethod()
        {
            ClientMethod(null,null);
        }

        public static void ClientMethod(Storage storage, string[] args)
        {
            Receiver receiver = storage!=null? new Receiver() {_storage =storage}: new Receiver();
            //CRUD команди, як окремі об*єкти
            _createCommand = new Create_Command(receiver) {arguments =args };
            _readCommand = new Read_Command(receiver) { arguments = args };
            _updateCommand = new Update_Command(receiver) { arguments = args };
            _deleteCommand = new Delete_Command(receiver) { arguments = args };

            invoker = new Invoker();
        }
        public static void CreateCommand()
        {
            invoker.StoreCommand(_createCommand);
            invoker.ExecuteCommand();
        }
        public static void ReadCommand()
        {
            invoker.StoreCommand(_readCommand);
            invoker.ExecuteCommand();
        }
        public static void UpdateCommand()
        {
            invoker.StoreCommand(_updateCommand);
            invoker.ExecuteCommand();
        }
        public static void DeleteCommand()
        {
            invoker.StoreCommand(_deleteCommand);
            invoker.ExecuteCommand();
        }
    }
}
