﻿using System.Collections.Generic;
using CRUD.Objects;
using Newtonsoft.Json;

namespace CRUD.Editors
{
    static class Json
    {
        public static string Serialize(this List<Person> library) => GetJsonString(library);
        private static string GetJsonString(List<Person> obj) => JsonConvert.SerializeObject(obj);
        public static object Deserialize(this string s) => GetObject(s);

        private static object GetObject(string obj)
        {
            return JsonConvert.DeserializeObject<List<Person>>(obj);
        }
    }
}
