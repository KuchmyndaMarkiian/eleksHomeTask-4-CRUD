﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRUD.Editors;
using CRUD.Objects;
using CRUD.Patterns.Chain;
using CRUD.Patterns.Interpreter;
using CRUD.Patterns.UsingCommand;

namespace CRUD.Crud
{
    public class Application
    {
        public enum Storage
        {
            Json,
            Xml
        }

        private FileEditor _editor;
        private string _format;

        /// <summary>
        /// Тип сховища
        /// </summary>
        Storage _storageType = Storage.Json;
        /// <summary>
        /// Сховище
        /// </summary>
        private Objects.Storage _storage;

        /// <summary>
        /// Ф-ія що роздупляє вхідну стрічку на команди та параметри. деталі у README у корені проекту
        /// 
        /// таки думаю тут можна включати паттерни Інтерпретатора чи Команди
        /// </summary>
        /// <param name="command">вхідний рядок з командами та параметрами</param>
        /// <returns></returns>
        public bool Execute(string command)
        {
            try
            {
                //на пустий рядок чи null
                if (string.IsNullOrEmpty(command) || !command.Any())
                    throw new Exception("Incorrect line");
                //поділ на підрядки по одинарному пробілу
                var lexems = command.Split(' ').Select(s => s.Trim()).ToArray();
                if (lexems.Contains("set") && lexems.Contains("storage"))
                {
                    SetStorage storageSetter = null;
                    switch (lexems[2])
                    {
                        case "xml":
                            storageSetter = new SetXmlStorage();
                            break;
                        default:
                            storageSetter = new SetJsonStorage();
                            break;
                    }

                    var setResult = storageSetter.Interpret();
                    _storageType = setResult.Item1;
                    _format = setResult.Item2;
                    return true;
                }

                Open(lexems[1]);

                //Command pattern
                Client.ClientMethod(_storage, lexems.Skip(2).ToArray());
                //Skip(2)- пропускаю перші 2 підрядка та повертаю решту .типу пропускаю команду та назву сховища, бо вони визначені

                //New Chain of Responcibility instanciated here
                //Should be using Dependecy Injection, but it's not
                 var chain = new Chain();

                //Add new AddHandler to chain: checking the 'add' command
                chain.Add(new AddHandler());
                //Add new DeleteHandler to chain: checking the 'delete' command
                chain.Add(new DeleteHandler());
                //Add new GetHandler to chain: checking the 'get' command
                chain.Add(new GetHandler());
                //Add new UpdateHandler to chain: checking the 'update' command
                chain.Add(new UpdateHandler());
                

                var command1 = lexems.First().Trim();
                if ((new string[]{"add","delete","update","get"}).Any(x=>x.Trim() == command1))
                {
                    chain.Message(command1);
                }
                Save();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Відкриття/Створення файлу у зажежності від SetStorage
        /// </summary>
        /// <param name="storage_name"></param>
        private void Open(string storage_name)
        {
            _editor = new FileEditor(_storageType);
            _editor.OpenOrCreate(storage_name + _format);
            _storage = new Objects.Storage() { Persons = _editor.Read() ?? new List<Person>() };

        }
        /// <summary>
        /// Збереження усього :-)
        /// </summary>
        private void Save()
        {
            _editor?.Write(_storage);
            if (_editor != null)
                Console.WriteLine("Executed");
        }
    }
}
