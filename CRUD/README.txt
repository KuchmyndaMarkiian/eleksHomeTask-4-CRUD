example:
set storage (xml or json)

add <storage> -firstname <text> -lastname <text>

delete <storage> -userId <id>

get <storage> -userId <id>

update <storage> -userId <id> (-firstname or -lastname)<new text>

