﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CRUD.Objects
{
    [Serializable]
    public class Storage
    {
        private List<Person> persons;

        public List<Person> Persons
        {
            get
            {
                persons.Sort();
                return persons;
            }

            set { persons = value; }
        }


        //Add
        public void Add(Person person)
        {
            //баг з айдішками
            if (persons.Any())
            {
                person.Id = Persons.Last().Id + 1;
            }
            Persons.Add(person);
        }

        //Delete
        public void Delete(int id)
        {
            Persons.Remove(Persons.FirstOrDefault(x => x.Id == id));
        }
    }
}
